# frozen_string_literal: true

require 'date'

module MissedResourceHelper
  GITLAB_RELEASE_DATE = 22

  def missed_resource?(milestone_due_date, release_date = release_date_for(milestone_due_date), current_date = Date.today)
    return false if current_date < milestone_due_date
    return true if current_date >= release_date

    current_date >= missed_resource_date_for(release_date)
  end

  def add_missed_labels(milestone_title, labels)
    result = [
      %Q{/label ~"missed:#{milestone_title}"}
    ]

    if labels.index('Deliverable')
      result << '/label ~"missed-deliverable"'
    end

    result.join("\n")
  end

  private

  def missed_resource_date_for(release_date)
    return release_date - 3 if release_date.monday? # delivery date on Friday
    return release_date - 2 if release_date.sunday? # delivery date on Friday

    release_date - 1 # delivery date on previous day
  end

  def release_date_for(due_date)
    Date.new(due_date.year, due_date.month, GITLAB_RELEASE_DATE)
  end
end
